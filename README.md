# Labor 2

Az alábbi repository egy előre beállított SpringBoot alkalmazást tartalmaz. Célja, hogy segítségével bemutatható legyen CI/CD pipeline-ok építése a GitLab-ben.

## Bevezetés

Az [előző labor](https://gitlab.com/b5483/labor1) során elkészítettünk egy GitLab projektet, melyhez hozzárendeltünk egy saját GitLab runner példányt, valamint készítettünk egy próba futtatást is, hogy ismerkedjünk a felülettel.

Ez az alkalom három részre tagolható:
- elindítjuk ismét a GitLab-runnert
- felpusholjuk az itt található alkalmazás forrást a saját repository-nkba
- építünk egy CI/CD pipeline-t

## A GitLab-runner újraindítása

Az előző alkalom során kértük, hogy tedd el magadnak a Runner beállítása során elkészült `config.toml` állományt. Itt az idő, hogy elővedd! Az előző alkalomhoz hasonlóan készíts egy gitlab nevű mappát, abba egy config mappát, és ebbe a config mappába másold be a korábbi `config.toml` állományt!

Indíts egy PowerShell-t (adminisztrátori módban), majd lépkedj a frissen készített GitLab mappába! Ha kiadsz egy `ls` utasítást, csak a config mappát kell látnod.

Másold be `egyesével` a parancssorba az alábbi három utasítást és üss entert mindegyik végén!

```sh
docker stop gitlab-runner
docker rm gitlab-runner

docker run -d --name gitlab-runner -v ${PWD}/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock quay.io/inotaykrisztian/gitlab-runner:latest
```

Az első utasítás leállítja az esetlegesen még futó gitlab-runner konténert, a második letörli, míg a harmadik újra inicializálja, immáron úgy, hogy az aktuális mappánkból kerül becsatolásra a config mappa.

Egy `docker ps` kiadása után látható lesz, hogy a konténer fut.

Ellenőrizzük, hogy a GitLab-be is felcsatlakozott!

Ezt ugynúgy tehetjük meg, ahogy az előző alkalmon tettük. Menj fel a gitlab.com-ra és jelentkezz be! A kezdőoldalon látnod kell a korábban létrehozott projekted. Klikkelj rá!

Ez után a Settings-ben válaszd a CI/CD szekciót!

<img src="images/cicd-settings.png">

A következő képernyőn pedig nyisd le a `Runners` elemet!

Az előző laboron beregisztrált runnert kell lásd és remélhetőleg a következő ábrával ellentétben zöld kör lesz előtte:

<img src="images/cicd-runner.png">

Ha pirosat látsz, akkor a konténer vagy nem indult el, vagy rosszak a beállításai. Ismételd meg az utasításokat a PowerShellben, és itt ellenőrizd, hogy zöld lesz a kör!

## Alkalmazás forrásának letöltése, majd pusholása a saját projektbe

Az [alábbi linken](https://gitlab.com/b5483/labor2/-/archive/main/labor2-main.zip) töltsd le a forráskódot, amit használni fogunk!

>>> Az alkalmazás, egy SpringBoot keretrendszerrel készített Java alkalmazás. Egyetlen dolgot tud, mégpedig ha megszólítjuk böngészőből, akkor visszaküldi, hogy Greetings from SpringBoot.
>>> Továbbá tartalmaz unit és integrációs teszteket, amelyeket egy folyamatban le is tudunk futtatni.
>>> Ezt az alkalmazást végül egy CI/CD lépés segítségével konténerizáljuk, és feltöljük a projekthez tartozó GitLab konténer image registry-jébe, majd pedig "kitelepítjük" a laborgépre.

Készíts egy mappát valahová, mindegy mi a neve, csak ne azt használd, amibe a runner konfigurációját tetted! Ebbe kell leklónozni egy git utasítás segítségével a projektet, amit a GitLab-en készítettel. Powershellben tallózz be ebbe a mappába, győződj meg róla, hogy üres!

A `GitLab-es saját projekteden` - tehát NEM a labor2 repon - a bal oldali menüben válaszd a `Repository-`t, majd jobb oldalon a kék `Clone` lenyitható gombot, és másold ki az URL-t, amit a `HTTPS-es` szekció tartalmaz!

<img src="images/git-repo-clone.png">

Powershell-ben add ki az alábbi utasítást az üres mappában:

```sh
git clone IDE-MÁSOLD-AZ-URL-T-GITLABRŐL
```

A klónozás megkezdése előtt fel fog ugrani egy ablak, ahol meg kell adnod a GitLab-es felhasználóneved és jelszavad, amikkel regisztráltál az első labor során.

A klónozás végén visszakapod a terminálban a kurzort. ELőször is egy ls kiadása után láthatod, hogy létrejött egy mappa. Lépj bele! A mappa neve a GitLab-es projekted neve. Ide húzta le a git a GitLab projekt tartalmát. Mivel az előző laboron már a develop branchre dolgoztunk, így most át kell válts arra, és onnét kell pullozd a repository tartalmát.

```sh
git checkout develop
```

Ha most kiadsz egy ls-t, akkor láthatod, hogy megjelent a fájlrendszerben az előző alkalom során létrehozott .gitlab-ci.yml állomány.

Amilyen módszerrel csak szeretnéd, csomagold ki a labor2 repository-ból letöltött zip állományt! Benne egy labor2-main nevű mappát fogsz találni. Annak a mappának a tartalmát (tehát ne a mappát, csak a tartalmát!) másold bele az imént inicializált gites mappába! Másoláskor a readme-t és a .gitlab-ci.yml-t felül kell írnod! Ha esetleg nem kérdezi meg a Windows, hogy akarsz-e fájlokat felülírni, akkor gyanakodj, hogy esetleg nem úgy másoltad át a fájlokat és mappákat, mint kellett volna! A másolás végén az alábbihoz hasonló mappa és fájl szerkezetet kell találnod:

<img src="images/own-repo.png">

Mindez után már csak arra van szükség, hogy az alkalmazáshoz tartozó állományokat, tehát mindent, amit az imént bemásoltál, felcommitold és pushold a GitLab-be! Ehhez térj vissza a terminálba, majd add ki az alábbi utasításokat, pont onnét ahonnét a korábbi git utasításokat is kiadtad!

```sh
git add .
git commit -m "added springboot app"
git push origin develop
```

Az utasítás végén egy összegző üzenet fog érkezni, hogy a feltöltés sikerült a GitLab-re. Válts böngészőre, és frissítsd a saját projekted oldalát. Látni fogod, hogy megjelentek az új mappák és fájlok a listában.

## A CI/CD pipeline megépítése

Ahogy azt az első laboron megismertük, az egyes CI/CD pipeline-okat a GitLab esetében a `.gitlab-ci.yml` állományban definiálhatjuk. Valójában az imént felpusholt forráskód is tartalmazott egy ilyen állományt, és a push hatására érvényre is jutott. Kezdésnek tartalmaz egy build lépést, ami lefordítja az alkalmazást, de mást egyelőre nem csinál. Ellenőrizzük, hogy lefutott!

Navigálj a CI/CD menü Pipelines pontjához!

<img src="images/cicd-pipelines.png">

Megjelenik egy lista, amely tartalmazni fogja az összes CI/CD futást, és a legelső elem az újonnan felpusholt kódhoz tartozó pipeline-t tartalmazza.

<img src="images/first-pipeline.png">

Elképzelhető, hogy nálad ez még running állapotban van.  A screenshot bal oldalán látható `passed` felirat valójában egy gomb. Nyomd meg! Mindegy mi a felirat rajta! Itt fogod tudni megnézni, hogy milyen Stage-ekből és Job-okból áll a jelenleg lefutott folyamat.

<img src="images/first-pipeline-jobs.png">

GitLab-ben a Pipeline-okat Stage-ekre és Job-okra osztjuk. A Stage az oszlop, a Job az elem az oszlopban. Jelen esetben a zöld pipás build a Job, és felette a nagy B betűs Build a Stage.

A Stage-ekbe tetszőleges mennyiségű Job tehető, de egy Job csak egyetlen Stage-be tartozhat. Azonos Stage-ben lévő Job-ok párhuzamosan futnak egymással, míg egy Stage-ben lévő Jobok futása csak akkor kezdődik, ha az azt megelőző Stage minden Job-ja lefutott már.

Tehát ebben a Pipeline-ban egyetlen lépés található, a build. Klikkelj rá! Ekkor láthatod, akár élőben is az aktuális lépés futásának a logjait.

<img src="images/build-logs.png">

A képernyőképen a sikeres build naplója látható. A legszembetűnőbb tétel a szöveg alján a zöld színű Job succeeded felirat. Ez jelzi, hogy amilyen utasításokat kiadtunk az adott lépésben, azok sikeresen lefutottak.

Az elkövetkező részekben rendszeresen fogjuk nézni a pipeline futások eredményeit, így javaslom, hogy legyen a böngészőben több lap is megnyitva. Egyiken legyen a repositoryban található forráskód, amihez rendszeresen hozzányúlunk, a másikon pedig a CI/CD menü pipelines nézete.

### A .gitlab-ci.yml fájl

A Repository/Files menüponttal navigálj a repo fájl listájára, majd válaszd ki a `.gitlab-ci.yml` fájlt! Minden leírás amit itt találsz szükséges ahhoz, hogy az imént megvizsgált build job lefusson. Nézzük végig tételesen!

```yaml
image: "quay.io/inotaykrisztian/openjdk:8u322"
```

A GitLab Runner-t az előző labor során Docker alapú feladatvégrehajtásra állítottuk be. Ez azt jelenti, hogy minden job egy mindentől független konténerben fog végrehajtásra kerülni. Ez azért nagyszerű, mert egyrészt a lépések nincsenek kihatással egymásra (külön hálózaton, külön fájlrendszerben futnak, mégis ugyanazon a gépen), valamint bármennyi lépésünk is van, mindegyik futhat teljesen más konténerben. Tehát lehetőségünk van arra, hogy minden lépéshez más és más eszközkészletet biztosítsunk. Erre láthatunk példát majd ezen a laboron is. A fenti sor annyit jelent, hogy minden lépés, amit definiálunk és nem rendelkezünk másképp, az openjdk image-ű konténerben fog futni. Ez az image tartalmaz mindent, ami Java alapú fejlesztéshez, fordításhoz szükséges. A tag-ben látható, hogy a JDK 8u322-es verzióját tartalmazza. Tehát nem telepítünk a gépünkre JDK-t, nem kell akár több verzióját is párhuzamosan fenntartsuk. Mindössze megmondjuk, hogy olyan konténerben fusson ez a lépés, amelyben ez az eszköz rendelkezésre áll.

```yaml
# global env variables, do not modify them!
.env_global: &env_global
  DOCKER_DRIVER: overlay
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true"

# local environment variables, can be extended
.env_local: &env_local    
  DOCKER_TLS_CERTDIR: ""

```

Az alábbi sorokban a GitLab és a yaml leírók sablonozási lehetőségeit használjuk ki. Definiálunk két olyan objektumot, amelyek valójában listák, és környezeti változókat tartalmaznak. Ezek a változók szükségesek az egyes lépésekhez, tételesen nem mennék rajtuk végig. Többnyire standard Maven (ez az eszköz fordítja le az alkalmazást) beállítások, nincs bennük semmilyen varázslat. A felhasználásukat a job-ban érhetjük tetten.

```yaml
default:
  before_script:
    - chmod +x mvnw 
```

Az alábbi szekció segítségével minden Job konfigurációjához hozzáadunk egy lépést. A `before_script` azt jelenti, hogy mielőtt lefuttatná a Runner az általunk kiadott utasítást, végrehajt egyéb lépéseket is. Ebben az esetben futtatási jogot adunk az mvnw nevű fájlnak. A konténerben Linux található, tehát ezért linuxos parancsot kell kiadni. A későbbi fordítási, tesztelési lépésekben mindvégig az mvnw alkalmazást fogjuk használni, emiatt feltétlenül szükséges, hogy egyáltalán ki tudjuk adni ezeket az utasításokat. Maga az mvnw pedig az ún. Maven wrapper. A Maven egy olyan szoftver, aminek a segítségével összetett Java alkalmazásokat lehet fordítani, tesztelni, stb. A Maven wrapper segítségével pedig beállítható, hogy mely Maven verzióra van szükség ezekhez a műveletekhez az adott forráshoz, és ez az alkalmazás le fogja tölteni a szükséges Maven verziót. Így nem nekünk kell ezt biztosítani, hanem megoldja a rendszer magának.

```yaml
stages: 
  - build 
```

Az alábbi szekció határozza meg, hogy milyen Stage-ek álljanak rendelkezésre a Pipeline jobjai számára. Jelenleg csak a build létezik.

A következő rész pedig maga a build job definíciója. Ezt részenként nézzük végig:

```yaml
build:    
```

Az első sorban határozzuk meg, hogy mi legyen a Job neve.

```yaml
    variables:
        <<: *env_local
        <<: *env_global 
```

A variables szekcióban környezeti változókat állíthatunk be, amelyek rendelkezésre fognak állni a Job futása során. A fájl legelején láthattuk ezeknek az objektumoknak a beállítását. Itt kerülnek felhasználásra. A sablonozásnak annyi az előnye, hogy ezek a beállítások több helyen felhasználásra kerülnek majd, vagyis célszerű egyetlen helyen beállítani őket, hogy ha változtatni kellene őket, akkor csak egy helyen kelljen ezt megtenni.

```yaml
    stage: build
```

A stage kulcsszóval rendelhetjük a Jobot egy stage-hez. Kötelező megadni.

```yaml
    cache:
        paths:
        - .m2/repository
        when: always
```

Lehetőségünk van az egyes pipeline-ok számára cache-t létrehozni. Ez arra szolgál, hogy gyorsítani tudja az esetleges függőségek letöltését a futások során. Például ennek az alkalmazásnak a fordításához több száz Java library-re van szükség. Ha ezt mindig minden lépésben letöltenénk, az rengeteg időbe kerülne. Ehelyett ha beállítjuk, hogy mely mappákat szeretnénk, hogy a GitLab megőrizzen nekünk, úgy akkor azokat mindig megkapja a Job, ahol ezt beállítottuk. A Maven ebben az esetben a letöltött libeket a .m2/repository mappában fogja elhelyezni, így ha azt hozzuk-visszük magunkkal a lépések között, akkor csak egyszer kell letölteni ezeket a függőségeket. A when: szekció pedig azt jelzi a GitLab számára, hogy ha esetleg hibás is lesz a Job futása, akkor is tegye el ezt a mappát.

```yaml
    tags:
        - docker
```

Az első laboron a Runner beállíása során meg kellett adni, hogy a Runner példányunknak milyen tag-jei legyenek. Ennek valójában itt a szerepe, ugyanis minden egyes Job-nak megmondhatjuk, hogy milyen tag-gel rendelkező Runner futtathatja. Például ha lenne egy nagyon komplex pipeline-unk, ahol kell Jobokat futtatni kifejezetten Windows-on, Mac-en és Docker-ben is, akkor pontosan így tehetjük ezt meg. Az adott gépekre feltelepítésre kerül a Runner, mindegyik megkapja a neki megfelelő tag-et, és az egyes Job-ok leírásában pedig meghatározzuk, hogy mely Runner-en fusson.

```yaml
    script:    
        - ./mvnw clean
        - ./mvnw compile
```

A script szekció tartalmazza tételesen azokat az utasításokat, amelyeket szeretnénk, hogy lefuttasson a Job. Az `mvnw clean` kipucolja az esetleges átmeneti fájlokat, és tiszta lapot ad a build számára, míg az `mvnw compile` pedig lefordítja az alkalmazást. Binárist még nem készít, csak fordítást végez.

```yaml
artifacts:
    paths:
      - target
    expire_in: 1 day
```

A legtöbb fordító rendszer készít átmeneti állományokat, vagy valamilyen eredménytermékeket, amelyek gyorsíthatják az egyes lépések közötti szükséges időt. Ebben mélyen nem szeretnék belemenni, de a Maven is így működik. Minden művelet során, amit vele végzünk, létre fog hozni egy target mappát, amelybe az átmeneti állományait és a reportjait fogja elhelyezni. Ha például kiadunk egy compile parancsot, majd rögtön utána egy verify-t (amely teszteket futtat), akkor a compile során keletkezett átmeneti állományokat felhasználva fogja végrehajtani a verify-t. Ha nem áll rendelkezésre a target mappa az átmeneti állományokkal, amikor elkezdődik a verify utasítás, akkor először végre fogja hajtani a compile-t újra, majd arra építve a verify-t. Vagyis valahogyan jó lenne, ha az egyes CI/CD Jobok között tudnánk fájlokat átadni. (Elspoilerezem, hogy lesz majd egy verify Job is, akinek kell a target mappa). Ugyanez a probléma szinte minden fordító rendszernél előjön. Emiatt a GitLab is biztosít erre lehetőséget az artifacts kulcsszó haszánlatával. Tehát, az artifacts segítségével lépések között, rövid tárolási idő mellett adunk át fájlokat. Ne keverjük össze a korábban ismertetett cache-sel, mert az minden Job-ra vonatkozik és hosszú távú tárolásra való! Az artifacts kulcsszó alatt látható, hogy mely mappát szeretnénk átadni egy következő lépésnek, illetve az is, hogy mennyi ideig tartsa meg a GitLab ezeket az átmeneti állományokat. Ez után az idő után automatikusan letörli őket.

```yaml
    rules:
        - if: $CI_COMMIT_TAG != null
        when: never
        - when: on_success
```

A build Job utolsó szekciója a rules. Ennél a kulcsszónál olyan szabályokat fogalmazhatunk meg, hogy mely esetben fusson le az adott Job. Itt azt láthatjuk megfogalmazva, hogy ha a CI/CD futás egy git tag felhelyezésével indult, akkor ne fusson le, egyébként viszont minden esetben. Ezt egyelőre ennyiben hagyjuk, később elmagyarázom.

Innentől kezdve az egyes lépések definícióiban az itt ismertetett elemeket már nem taglalom részletesen, mindenhol ugyanaz a szerepük.

### A megvalósítandó CI/CD folyamat

A folyamat, ahogy a neve is utal rá, lépések sorozatából tevődik össze. Két folyamatot fogunk megvalósítani: egy CI-t és egy CD-t. 

A CI folyamat lefordítja és lefuttatja a teszteken a kódot, míg a CD folyamat előállítja a binárist a forráskódból, ami már futtatható önállóan, készít belőle egy Docker image-et, feltölti a GitLab repository-jába, végezetül pedig a saját laborgépünkön elindítja ezt az image-et, és mi magunk is kipróbálhatjuk az immáron konténerben futó alkalmazást.

A CI folyamat:
<img src="images/ci-pipeline.png">

A CD folyamat:
<img src="images/cd-pipeline.png">

### Test Stage és Job

Egészítsük ki a .gitlab-ci.yml állományt, hogy tartalmazzon egy Stage-et és egy Job-ot a forrásban található unit és integrációs tesztek lefuttatásához!

Ehhez navigáljunk a Repository/File nézetbe, győződjünk meg róla, hogy a develop branchen vagyunk, és nyomjuk meg a Web IDE gombot a jobb felső sarokban!

<img src="images/web-ide.png">

A gomb megnyomásának hatására kapunk egy webes szerkesztőt, így nincs szükségünk külön eszközök használatára ahhoz, hogy dolgozzunk a git-ben. 

<img src="images/web-ide-ci.png">

A szerkesztő elég intuitív. Bal oldalon a fájlok, jobb oldalon a fájl tartalma. Válaszd ki bal oldalon a .gitlab-ci.yml-t!

Először is felveszünk egy új Stage-et, amiben a teszt job fog futni. Keresd meg a `stages` részt a fájlban és egy-az-egyben cseréld ki erre:

```yaml
stages: 
  - build
  - test 
```

Végezetül illeszd be az új job leírását a fájl végére! Ügyelj arra, hogy a kurzor az üres sor legelején legyen!

```yaml
test:  
  variables:
    <<: *env_local
    <<: *env_global    
  stage: test
  cache:
    paths:
      - .m2/repository
    when: always
  tags:
    - docker
  script:    
    - ./mvnw verify
    - awk -F"," '{ instructions += $4 + $5; covered += $5 } END { print covered, "/",instructions, "instructions covered\n"; print 100*covered/instructions, "% covered" }' target/site/jacoco-aggregate/jacoco.csv
  artifacts:
    paths:
      - target
    expire_in: 1 day
    when: always
    reports:
      junit:
        - "target/surefire-reports/TEST-*.xml"  
        - "target/failsafe-reports/TEST-*.xml"  
  coverage: '/^\d+.\d+ \% covered/'
  rules:
    - if: $CI_COMMIT_TAG != null
      when: never
    - when: on_success  
```

Van néhány tétel ebben a leíróban, amit nézzünk meg közelebbről!

Az alkalmazás forrásában úgy állítottuk be, hogy számoljon tesztlefedettség értéket is a futások alapján a Maven egy JaCoCo (java code coverage) nevű plugin felhasználásával. Minden teszteredmény a target mappába fog kerülni a futás végén. A GitLab lehetőséget ad mind a teszteredmények, mind a lefedettségi adatok megjelenítésére a futás után, így ezeket kössük is be hozzá!

A `scriptben` látható, hogy az mvnw verify utasítást adja ki. A Maven ebben az esetben le fogja futtatni mind a unit, mind az integrációs teszteket. Az utána következő AWK-s utasítás elég ronda, és nem is szeretnénk belemenni, de annyit csinál, hogy a kódlefedettségi reportból megkeresi a lefedettség értékét %-ban és egyszerűen kiírja a konzolra futás közben. (és csak megnyugtatásként: ezt az utasítást mi is úgy gúgliztuk ki, nem fejből van :D )

Az `artifacts` szekcióban egy új rész jelent meg, mégpedig a `reports`. Ahogy emlíettem, a GitLab képes "felszedni" a teszteredményeket, és ezt ebben a szekcióban lehet megadni neki. A leírás annyit jelent, hogy a megadott két mappán belül JUnit típusú reportokat fog találni, és értelmezzen minden olyan fájlt, ami illeszkedik a TEST-*.xml mintára. Így a GitLab képes lesz megjeleníteni a fejlesztők számára mind a sikeres eredményeket, mind a hibásakat is. Gondoljunk bele! A fejlesztőnek nem kell letöltenie ezeket a reportokat, nem kell magánál reprodukálnia a hibát, a GitLab pontosan meg fogja mutatni mindig, hogy milyen teszt nem tudott lefutni és azt is, hogy miért!

Még egy eddig nem látott elem található a leírásban, a `coverage`. Ahogy említettem, a GitLab fel tudja szedni a tesztlefedettség értékét is, és számos helyen képes azt megjeleníteni a felületén. Ehhez arra van szükség, hogy a lefedettségi érték szerepeljen kiírva a konzolon (erre való a ronda awk-s utasíás), valamint meg kell adni egy reguláris kifejezést, aminek a segítségével a GitLab megtalálja, hogy az adott sorban a lefedettségi érték található. Ezt a reguláris kifejezést tartalmazza a `coverage` szekció.

Most, hogy hozzáadtad az új Jobot, és látod, hogy mit csinál, itt az ideje felpusholni a módosítást a gitbe! Ehhez csak meg kell nyomni bal oldalt lent a `Commit` gombot.

<img src="images/web-ide-commit.png">

Commit message-nek írj be valamit, pl. added test job. A commit to develop branch maradjon így, viszont a start a new merge request-ből vedd ki a pipát! Nem szeretnénk ilyet indítani.

Felhívom a figyelmed, hogy abban a pillanatban, ahogy megnyomod a commit gombot ismét, érvényre jutnak a módosításaid és elkezd futni a CI pipeline, immáron két Jobbal. Úgyhogy nyomd meg a gombot, és egyből navigálj is a CI/CD menü pipelines részébe!

Itt láthathatsz egy új sort, ahol már látszik, hogy két stage-ből áll a pipeline. Ha rányomsz a Running gombra, akkor láthatod, hogy megjelent a Build mellett a Test stage és Job is.

<img src="images/ci-pipeline.png">

Valahogy kb így. Ha ráklikkelsz a build és a test lépésekre, akkor láthatod a naplóit futás közben. Először a build fog lefutni, majd utána a test indul el. A test sikeres futásának a vége kb. így néz ki:

<img src="images/test-log.png">

Pirossal aláhúztam pár érdekes tételt a logban. Az első, a coverage érték. Látható a logban, hogy 88.6%-ot írt ki az awk-s utasítás a konzolra, és jobb oldalon a fehér hátterű sávban a GitLab ezt már fel is dolgozta, és ugyanaz az érték látható benne. Ez azt jelenti, hogy innentől kezdve a GitLab tényleg képes értelmezni ezt az adatot és mutatja is.

Az alsó 3 sor pedig azt mutatja, hogy megtalálta a target mappában a teszteredményeket és azokat is feltöltötte magának. 

Navigáljunk vissza a pipeline nézetbe! Vagyis CI/CD menü, pipelines, majd a legelső `passed` zöld gombra! Ekkor jutunk vissza oda, hogy láthatjuk a stage-ekben a Jobokat. 

<img src="images/tests-in-pipelines.png">

Láthatjuk, hogy a Tests sávnál megjelent egy 2-es, vagyis két tesztet tartalmazott a pipeline futása. Nyomd meg!

<img src="images/tests-in-pipelines-details.png">

Ha kiválasztottuk, akkor láthatunk egy táblázatot, amelyben szerepel, hogy a Test nevű Jobunkban futott 2 darab sikeres teszt. Nyomd meg a Test listaelemet!

<img src="images/tests-in-pipelines-details2.png">

Ekkor már részletesen is láthatjuk a teszteredményeket. Itt szerepel a teszt neve, futási ideje, eredménye. Amennyiben sikertelen lett volna a teszt, akkor itt láthatnánk azt is, hogy pontosan miért nem sikerült. Könnyen belátható, hogy mivel ezeket a felületeket az alkalmazás fejlesztők is elérik, nagyon meg tudja könnyíteni a fejlesztési folyamatot, hogy ilyen szinten követhetővé válik például a tesztek futtatása.

### Egy merge request

Nézzük meg, hogy az általunk eddig készített pipeline hogyan befolyásolja a fejlesztők mindennapi munkáját!

Bal oldalon a menüben keresd meg a Merge requests pontot és klikkelj rá! A megjelenő oldalon egyetlen gombot fogsz látni, Create merge request névvel. Nyomd meg!

>>> A merge request a GitLab-ben egy olyan eszköz, ahol lehetőségünk van az egyes git branchek másikba behúzására igényt feladni. A gyakorlatban a szoftverfejlesztés általában olyan szabály szerint történik, hogy a fejlesztők tetszőleges branchet készíthetnek maguknak, de azt visszahúzni a developba, vagy a main-be már nem tudják megtenni. Ilyenkor kell pull vagy merge requestet feladniuk. Amikor feladnak egy ilyet, tipikusan a vezető fejlesztő kap egy értesítést, és ő itt végezheti el a code review-t, aminek során eldönti, hogy megfelelő minőségű-e az a módosítás, amit a feladó vissza szeretne tenni a nagy közösbe. Tehát ezen a képernyőn minél több információt tudunk megjeleníteni egy-egy CI folyamat futása során, annál több információ fog rendelkezésre állni a merge request elbírálása során.

A megjelenő dialógusban először ki kell válasszuk, hogy melyik branchet szeretnénk melyik másikba behúzni. Itt válasszuk ki a developot bal oldalon, míg a main-t jobb oldalon.

<img src="images/create-mr-branches.png">

Nyomjuk meg a compare branches and continue gombot!

A megjelenő képernyőn adhatnánk címet, leírást az MR-nek. Mi ezt most hagyjuk így ahogy van, a lap alján keresd meg a Create merge request gombot és nyomd meg! Ekkor megjelenik a létrehozott Merge Request-ünk. Figyeljünk meg, hogy minden, amit eddig összegyűjtöttünk a CI futás során, látható benne!

<img src="images/merge-request.png">

Az első a piros négyzettel bekeretezett két zöld pipa. Ez a lefuttatott pipline stage-eit mutatja. Azonnal látható, hogy a folyamat sikeresen lefutott. 

Balról és felülről az első piros aláhúzásnál látható, hogy mekkora a mért tesztlefedettségünk. Ha a cél branchnél is futott volna már pipeline, és ott is lenne egy mért értékünk, akkor a GitLab ugyanitt azt is megmutatná, hogy mennyivel fog változni a lefedettség, ha az adott branchet behúzzuk a célbranchbe. 

Végezetül a legalsó piros vonal felett látható, hogy a GitLab 2 teszt futást talált, és mindkét teszt rendben lefutott. Ha valamelyik teszt sikertelen lett volna, úgy itt láthatnánk azt az Exception-t is, amelyet a teszt generált a futása során, vagyis innét azonnal lehet látni, hogy mivel és mi baj történt.

Nagyon röviden összefoglalva ez minden CI folyamat lényege. Minél több, minél értékesebb eredményt szolgáltató tesztet, elemzést, ellenőrzést elvégezni automatizáltan, majd jelezni a fejlesztőknek, hogy hol van esetleg probléma az aktuális módosításukkal.

Nyugodtan el lehet navigálni erről a képernyőről, nem kell elfogadni a Merge Requestet, ez a képernyő csak szemléltetésre szolgált.

>>> A GitLab számtalan reportot tud ezen a felületen megjeleníteni, és látható, hogy mennyivel egyszerűbbé teheti egy jól elkészített CI/CD folyamat a munka ellenőrzését és nyomon követését.

### A CD folyamat

A CD folyamat során általában elkészítjük automatizáltan az aktuális forrásból a release-ünket, vagyis kiadjuk a szoftvert a pillanatnyi állapotában. Ezek a release-eket valahol el kell tárolnunk, ez most Docker image-ként fog megtörténni. Utolsó lépésben pedig automatizáltan ki is telepítjük azt oda, ahol futni fog, és el is indítjuk. Mindezt emberi beavatkozás nélkül.

A CD folyamathoz 3 új stage-et és 3 új jobot vezetünk be. Ezt egyben fogjuk hozzáadni a .gitlab-ci.yml állományhoz.

Menj vissza a Web IDE-be, és először is cseréld le a `stages` szekciót erre:

```yaml
stages: 
  - build
  - test  
  - package
  - release
  - deployment
```

Láthatod, hogy 3 új Stage került be a folyamat leíróba. A package-ben elkészítjük a futtatható binárist a forrásból, a release során Docker image-be fogjuk csomagolni, míg a deployment részben "kitelepítjük" az elkészült image-et.

A fájl végén egy üres sorba másold be az alábbit:

```yaml
package-jar:
  stage: package  
  variables:
    <<: *env_local
    <<: *env_global
  cache:
    paths:
      - .m2/repository
    when: always
  tags:
    - docker
  script:    
  - ./mvnw versions:set -DnewVersion=$CI_COMMIT_TAG
  - ./mvnw package -DskipTests
  artifacts:
    paths:
      - target
    expire_in: 1 day   
  rules:
    - if: $CI_COMMIT_TAG =~ /^DEV-.*$/
      when: on_success
    - when: never
```

Ez a Job fogja egy Maven utasítással elkészíteni a forráskódból a futtatható binárist, vagyis egy jar fájlt. Az eddig látottakhoz képest a `scriptben` illetve a `rules`-ban található változás. Nézzük fordítva!

A `rules`-ban határozzuk meg, hogy mikor fusson egy adott lépés. Eddig az volt a feltételünk, hogy mindig fussanak a job-ok, amennyiben a folyamat futását nem egy git tag felhelyezése triggerelte. Itt ez változik, ez a lépés csak akkor futhat, ha git tag felhelyezés hatására indult el a folyamat, és a tag neve matchel a DEV-* regexre. Vagyis minden alkalommal futni fog, amikor feltesztünk egy DEV-BÁRMI kezdetű tag-et. Ha belegondolunk, akkor valahogyan meg kell tudni különböztetni, hogy mikor fusson a CI folyamat és mikor fusson a CD folyamat. A CI minden commit esetében kell fusson, amikor a kód változott és ellenőrizni kell, míg a CD-nek csak akkor, ha ki szeretnénk adni az alkalmazásból egy változatot. Vagyis akkor ha a kiadási szándékot egy git tag felhelyezésével jelezzük, és a tag-be mondjuk beleírjuk, hogy mely környezetbe és milyen verziószámmal szeretnénk kiadást indítani, akkor ez lehet az, amivel szeparálni a CI és a CD folyamatokat egymástól. Jelen esetben, ha felhelyezzük a DEV-0.0.1 tag-et, akkor egyrészt a gitben megjelöltük ezt a commitot, hogy ebből készült a 0.0.1-es változat, valamint azt is mellé csempésztük, hogy az itt készült alkalmazást a DEV környezetbe szánjuk.

A `script` szekcióban pedig két utasítást adunk ki. Az első egy kényelmi utasítás, és azért van itt, hogy szemléltesse milyen sok repetitív munkától tud megszabadítani bennünket egy jól megtervezett folyamat. Az első sor
`- ./mvnw versions:set -DnewVersion=$CI_COMMIT_TAG` egy Maven plugin segítségével átírja a forráskódban a szoftver verziószámát, amit készítünk. Azt fogja beállítani még fordítás előtt, amit a git tag-be írtunk. Tehát amikor a folyamat a DEV-0.0.1 tag felhelyezésével indult, akkor mi automatikusan egy olyan alkalmazást fogunk fordítani, aminek pontosan ugyanez a verziószáma. Ezt máskülönben a fejlesztőnek kézzel kellene követnie a forrásban. Így viszont erre már nincs semmilyen szükség, hiszen bármelyik commitot lehetséges bármilyen verziószámmal release-elni. A script szekcióban a második sor pedig egy teljesen átlagos Maven utasítás, jelzi a fordítónak, hogy készítsen egy futtatható binárist a forrásunkból.

Fontos lehet megemlíteni, hogy az `artifacts` szekcióban mondtuk meg, hogy az egyes Jobok között szeretnénk állományokat átadni. Az elkészült bináris a target mappába kerül a Maven által, pont oda ahová korábban a reportok is kerültek. A binárist felhasználjuk a következő lépésben, emiatt kell vinnünk azt magunkkal.

Üss pár entert a bemásolt Job leírója alá, és másold oda a következőt:

```yaml
containerization:  
  image: "quay.io/inotaykrisztian/docker:latest"
  variables:
    <<: *env_local
    <<: *env_global
    DOCKER_REGISTRY: $CI_REGISTRY_IMAGE
  stage: release
  tags:
    - docker  
  script:
    - docker build -f Dockerfile -t $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG .
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
    - docker rmi $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  rules:
    - if: $CI_COMMIT_TAG =~ /^DEV-.*$/
      when: on_success
    - when: never
```

Azt a lépést, amikor egy alkalmazást docker image-be csomagolunk, konténerizálásnak nevezzük. Ez a lépés is ezt a feladatot hivatott megvalósítani. A GitLab alapértelmezetten, minden projekthez tartalmaz egy szabadon felhasználható Docker image registry-t, ahol tárolhatjuk az általunk elkészített image-eket. Mi is ezt fogjuk tenni. Elkészítünk egy Docker image-et, amely tartalmazza az alkalmazásunkat, illetve azt, hogy hogyan kell elindítani, majd feltöltjük a GitLab projektünk registry-jébe, hogy eltároljuk.

Figyeld meg, hogy itt explicit megmondtuk, hogy milyen image-ben szeretnénk a lépést végrehajtani! Azért tettük ezt, mert ez idáig minden egyes lépést egy JDK-t tartalmazó image-ben hajtottunk végre, mert a JDK-val dolgoztunk. Viszont itt a futtatható alkalmazásunk már előállt, nincs további szükségünk a JDK-ra. Viszont Docker image-et szeretnénk előállítani, így egy olyan konténerben kell dolgozzunk, amiben már nem JDK, hanem Docker található meg. Illetve... a Docker parancssori utasításai találhatóak meg. Az első laboron a Runner-t úgy állítottuk be, hogy a konténerekben lévő Docker eszközök a hoszt gépen telepített Docker-t tudják közvetlenül használni. Tehát amit itt docker utasítást kiadunk, az a konténerben lévő cli eszközzel kerül végrehajtásra, de ő a Windows-unkon telepített Docker-t hívja meg valójában.

A GitLab a pipeline-ok futása során rengeteg környezeti változót biztosít számunkra, amiket felhasználhatunk a Jobokban. Ilyen volt az is, amivel a git tag értékét átvettük, hogy verziószám legyen belőle. A mostani `script` szekcióban is sok $-lal keződő csupa nagybetűs sztringet láthattok. Ezek is mind ilyenek. Ezek segítségével megkaphatjuk futás közben a projekthez tartozó docker registry URL-jét, valamint hozzá egy átmeneti felhaználó nevet és jelszót is.

A `script`-ben az első sor elkészíti a binárisunk felhasználásával a docker image-et, aminek a nevét dinamikusan állítja elő: GITLAB_PROJEKT_REGISTRY_URL:GIT_TAG formátumban. A Docker image-ek nevei tartalmazzák az URL-t is, ahonnét le/fel lehet tölteni őket. A `docker build` utasítás a projektben található `Dockerfile`-t használja, amelyben azok az utasítások találhatóak, hogy hogyan kell egy ilyen image-et elkészíteni. Ennek felhasználásával összerakja az image-et és lokálisan elérhetővé is teszi.

A többi sor pedig arra szolgál, hogy fel is tudjuk tölteni ezt a registry-be. Először be kell jelentkezni ide (hiszen akárki akármit nem tölthet csak úgy fel), ezt végzi a `docker login`. Az immár lokálisan kész image-et a registry-be tölti fel a `docker push` utasítás. Végezetül ezt rengetegen elfelejtik, de az elkészült image-ek minden esetben tárhelyet foglalnak azon a gépen, ahol készültek. Takarítsunk is ki magunk után! A `docker rmi` utasítás letörli a gépről az image-et, így azzal már nem kell foglalkozni a jövőben karbantartás címszóval.

Az utolsó Jobunk leírása következik. Üss néhány entert az előző után, és másold be a következő szekciót:

```yaml
deploy:   
  stage: deployment  
  image: "quay.io/inotaykrisztian/docker:latest"  
  tags:
    - docker
  variables:
    <<: *env_local
    <<: *env_global
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker run -d --rm -p 8080:8080 $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  rules:
    - if: $CI_COMMIT_TAG =~ /^DEV-.*$/
      when: on_success
    - when: never   
```

A CD folyamat legutolsó lépése el fogja indítani a saját laborgépen az előző lépésekben elkészített Docker image-et. Ehhez igazából két utasításra, és egy apró trükkre van szükség. Nézzük először a `script` szekciót!

Az első utasítás, pontosan úgy, ahogy az előző lépésben, be fog jelentkezni a docker cli-vel a GitLab projekt Docker registry-jébe. Erre azért van szükség, hogy le tudjuk tölteni az imént elkészített image-et. Ne feledjük, hogy minden Job külön konténerekben fut, vagyis ha most egyszerre párhuzamosan 100 jobunk futnak, akkor is mindegyikben be kellene jelentkeznünk, hiszen ezekre úgy kell tekinteni, mintha egy éppen ebben a pillanatban feltelepített, teljesen üres számítógépek lennének.

A második utasítás pedig nagyon hasonlít arra, mint amivel az első labor során ellenőriztük, hogy működik a gépünkön a Docker maga. A sor végén látható, hogy azt az image-et használja, amit az imént feltöltött (változókból értékeli ki a nevet ugye). Egyszerűen fogja, és elindítja az image-et, és a 8080-as TCP portját kiengedi a saját gépünkre. Vagyis elindítja a konkrét konténert és megoldja, hogy a 8080-on meg tudjuk szólítani böngészőből.

Viszont akkor mi a trükk? Gondoljuk át! Egy konténerben elindítok egy új konténert, mégis a saját gépemen fog elindulni. Ez pont ugyanaz, mint ahogy a hoszt gépen futó Docker építette meg az előző lépésben az image-et valójában. A GitLab Runner configjába beillesztettük ezt a sort:

```
volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
```

Ez a sor utasítja a GitLab Runner-t, hogy minden Job-ként futó konténerbe mountolja be a hosztgépen található /var/run/docker.sock -ot. Ez pedig, ahogy korábban említettem, maga a Docker szolgáltatás TCP portja. Vagyis amikor a docker utasítás a rendszerben található docker socketet próbálja meg elérni, akkor az általunk  hoszt gépről becsatolt socketet találja meg, tehát direktben fogja használni a hosztgépen található Docker szolgáltatást. Így lehetséges, hogy ha egy konténertben a Runnerben elindítunk egy másik konténert, akkor az a hoszt gépen kezd el futni.

Tehát, a `.gitlab-ci.yml` leíró gazdagabb lett 3 új Jobbal. Commitoljuk fel! Pontosan ugyanúgy, ahogy korábban tettük. Commit gomb bal oldalt, nem szeretnénk új merge requestet indítani, és ismét commit!

Minek kellett most akkor történnie? Az új CD folyamatunk nem indulhatott el, hiszen azt egy git tag felhelyezése fogja indítani. Azonban commitoltunk egyet, így a CI folyamat már fut is, igaz, most az alkalmazás kódján nem változtattunk semmit.

Indítsuk el a CD folyamatot! Ehhez először a bal oldali menüben válaszd ki a Repository/Tags elemet!

<img src="images/git-tags.png">

A megjelenő képernyő jobb felső sarkában találsz egy "New tag" nevű kék gombot. Nyomd meg!

Ahogy említettem korábban, a tag-et commitra tesszük rá. Git kliens szoftverben tetszőleges commitra fel tudjuk ezt tenni, de a GitLab esetében egy adott branch éppen legutolsó commitja lesz majd a cél. Ez nekünk természetesen rendben van. A `Tag name` legyen `DEV-0.0.1` A job leírásakor láthattuk, hogy egyrész a DEV-VALAMI minta fogja elindítani a CD folyamatot, másrészt a tag neve lesz az alkalmazás verziószáma és a készítendő konténer image neve is. Minden tag nevének egyedinek kell lennie az egész projektben, így ez garantálni is fogja nekünk, hogy ugyanazt a tag-et 2x ne tudjuk feltenni, ezáltal nem tudjunk több image-et készíteni azonos verzióval. A `Create from` legördülőben válasszuk ki a `develop` branchet! Ide fog felkerülni a tag. A lap alján nyomd meg a `Create tag` gombot! Ez által a tag létre is jött.

Navigálj a CI/CD / Pipelines menübe!

Azt fogod tapasztalni, hogy a legfelső elem az immáron futó CD pipeline-unk.

<img src="images/cd-pipeline-summary.png">

Látható a 3 karikából, hogy már 3 stage-ben találhatóak Jobok. Klikkelj a sor elején a `Running` gombra! (ha később teszed, akkor lehet, hogy már `passed` lesz belőle)

<img src="images/cd-pipeline-jobs.png">

Láthatod, hogy megjelent a 3 új Jobunk. Ezekre ráklikkelve a megszokott módon meg lehet nézni a naplóját a futásnak.

A package-jar logjában látható, hogy elkészült a jar fájl, és ha kicsit feljebb görgetsz, akkor azt is láthatod, hogy a git tag neve is a fájlnév részévé vált! Azért, mert ezt használtuk fel verziószámnak a fordítás során.

<img src="images/jar-version.png">

A containerization lépésben tételesen végig lehet nézni, hogy mely docker utasításnak mi lett a kimenete, és ahogy lépésről lépésre elkészül, majd felkerül a registry-be az image-ünk.

<img src="images/docker-image-push.png">

A pirossal aláhúzott sor mutatja, hogy az image-et felpusholta a lépés a GitLab registry-jébe. Amikor ezt látod, nézzük is meg, hogy hova került!

Ehhez a Packages & Registries menü Container registry pontját válaszd ki!

<img src="images/registry.png">

A megjelenő oldalon láthatod, hogy milyen image-ek készültek eddig el:

<img src="images/registry-summary.png">

A pirossal kihúzott rész helyén a saját GitLab projekted elérését láthatod, és alatta, hogy hány image tag készült el eddig. Ez Nálad 1 lesz. Klikkelj a sorra, aminek a root image a vége! Ekkor megnézhetjük az eddig elkészített tag-eket. Figyelem! Ez a tag itt nem git tag-et, hanem docker image tag-et jelent! A két fogalom nem ugyanaz! 

<img src="images/registry-images.png">

A megjelenő felületen pedig láthatod az image-et, amit elkészített automatizáltan a CD folyamat, és fel is töltötte ide. Ezen a felületen van lehetőség mindezt átnézni, vagy image-eket törölni.

Navigálj vissza a CD pipeline-hoz és keresd meg a `deploy` nevű Jobot! Ő az, aki a saját gépünkön el is indítja ezt a konténert, amit készítettünk. 

<img src="images/deploy-success.png">

A naplójában látható, ahogy először beloginol a docker cli-vel a projekt Docker registry-jébe, majd kiadja a run utasítást, amivel el fogja indítani a konténert a saját gépünkön. Látható, ahogy immár a saját gépünk letölti a konténert, majd el is indítja és ezzel a folyamatnak vége is.

Nincs más dolgunk hátra, mint hogy ellenőrizzük böngészőből, hogy tényleg fut-e. Egy üres lapon írd be ezt az URL-t: http://localhost:8080

Ezt kell lásd:
<img src="images/greetings.png">

Ezzel a labor végére értél. Utolsó lépésben kérlek állítsd le a futó GitLab runner-t  a gépen. Ezt Powershellben a `docker stop gitlab-runner` utasítással teheted meg.